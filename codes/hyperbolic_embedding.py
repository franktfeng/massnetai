import os, random, time
from argparse import ArgumentParser, FileType, ArgumentDefaultsHelpFormatter
from deepwalk import graph
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from mpl_toolkits.mplot3d import Axes3D

class HyperbolicEmbeddding:
    def __init__(self, 
                 folder,
                 graph_format,
                 undirected,
                 num_walks,
                 walk_length,
                 window_size,
                 dim,
                 seed,
                 epoch,
                 euclidean,
                 batch_size):
        self.folder_name = folder
        self.graph_file = 'data/{}/graph.txt'.format(folder)   
        self.color_file = 'data/{}/colors.npy'.format(folder)   
        self.color = np.load(self.color_file)
            
        self.graph_format = graph_format
        self.undirected = undirected
        self.num_walks = num_walks 
        self.walk_length = walk_length
        self.window_size = window_size
        self.seed = seed
        self.epoch = epoch
        self.euclidean = euclidean
        self.batch_size = batch_size
        self.D = dim
        self.G = None
        self.epsilon = 1e-5
        
        self.R = 1
        self.SqrtR = np.sqrt(self.R)
        self.r = 1
        self.gamma = 10
        
        print('--> Initialising')
        
        if self.graph_format == 'adjlist':
            self.G = graph.load_adjacencylist(self.graph_file, undirected=self.undirected)
        elif self.graph_format == 'edgelist':
            self.G = graph.load_edgelist(self.graph_file, undirected=self.undirected)
        else:
            raise Exception('Unknown file format.')
            
        if self.batch_size < 1:
            self.batch_size = 1
            
        print('---')
        
    def getNN(self, X, W, B):
        l_1 = tf.add(tf.matmul(X, W['h1']), B['b1'])
        l_2 = tf.add(tf.matmul(l_1, W['h2']), B['b2'])
        l_3 = tf.add(tf.matmul(l_2, W['h3']), B['b3'])
        l_4 = tf.add(tf.matmul(l_3, W['h4']), B['b4'])
        l_5 = tf.add(tf.matmul(l_4, W['h5']), B['b5'])
        l = l_5
        return l
        
    def embedToDome(self, Y, V, m=50, radius=1.0, num_epochs=100, learning_rate=0.1, drop=0.99, drop_epochs=1):
#        embedding_start_time = time.time()
#        embedding_start_time = int(round(embedding_start_time))
        
        embedding_folder = 'dome-{}'.format(self.folder_name)    
        if not os.path.exists(embedding_folder):
            os.makedirs(embedding_folder)
                
        W = {
            'h1': tf.Variable(tf.random_normal([self.D, 32])),
            'h2': tf.Variable(tf.random_normal([32, 16])),
            'h3': tf.Variable(tf.random_normal([16, 8])),
            'h4': tf.Variable(tf.random_normal([8, 4])),
            'h5': tf.Variable(tf.random_normal([4, 3]))
        }
        
        B = {
            'b1': tf.Variable(tf.random_normal([32])),
            'b2': tf.Variable(tf.random_normal([16])),
            'b3': tf.Variable(tf.random_normal([8])),
            'b4': tf.Variable(tf.random_normal([4])),
            'b5': tf.Variable(tf.random_normal([3]))
        }
        
        tf_alpha = tf.placeholder(dtype=tf.float32)
        
        tf_X1 = tf.placeholder(shape=[None, self.D], dtype=tf.float32)
        tf_X2 = tf.placeholder(shape=[None, self.D], dtype=tf.float32)
        tf_X = tf.placeholder(shape=[None, self.D], dtype=tf.float32)

        tf_Zeta = 1 + 2 * self.R * tf.pow(tf.norm(tf_X1 - tf_X2, axis=1), 2) / (self.R - tf.pow(tf.norm(tf_X1, axis=1), 2)) / (self.R - tf.pow(tf.norm(tf_X2, axis=1), 2))
        tf_DistP = tf.acosh(tf_Zeta)
        tf_DistP = tf_DistP / tf.reduce_max(tf_DistP)
        
        
        tf_Y1 = self.getNN(tf_X1, W, B)
        tf_Y2 = self.getNN(tf_X2, W, B)
        tf_Y = self.getNN(tf_X, W, B)
        
        tf_Z1 = tf.nn.l2_normalize(tf_Y1, axis=1)
        tf_Z2 = tf.nn.l2_normalize(tf_Y2, axis=1)
        tf_Z = tf.nn.l2_normalize(tf_Y, axis=1)
        
        tf_Cos = tf_Z1 * tf_Z2
        #tf_Cos = tf.reduce_sum(tf_Cos, axis=1)
        tf_Cos = tf_Cos[:, 0] + tf_Cos[:, 1] + tf.abs(tf_Cos[:, 2])
        tf_Cos = tf.clip_by_value(tf_Cos, clip_value_min=-1+self.epsilon, clip_value_max=1-self.epsilon)
        # tf_DistD = tf.acos(tf_Cos) * radius
        tf_DistD = tf.acos(tf_Cos) / np.pi
        
        tf_DeltaDist = tf_DistP - tf_DistD
        tf_Loss = tf.reduce_sum(tf.pow(tf_DeltaDist, 2))
            
        optimiser = tf.train.AdamOptimizer(learning_rate=tf_alpha).minimize(tf_Loss)
        init = tf.global_variables_initializer()
        
        epochs = np.zeros(num_epochs+1, dtype=np.int32)
        losses = np.zeros(num_epochs+1, dtype=np.float32)
        alphas = np.zeros(num_epochs+1, dtype=np.float32)

        with tf.Session() as sess:
            sess.run(init)
            
            V2 = np.copy(V)
            np.random.RandomState(self.seed).shuffle(V2)
            S = V2[0:m]
            U = Y[S, :]
            
            X1 = None
            X2 = None
            for i in range(0, m-1):
                x_range = range(i+1, m)
                x1 = np.reshape(np.tile(U[i, :], len(x_range)), (len(x_range), self.D))
                x2 = U[x_range, :]
                
                if i == 0:
                    X1 = x1
                    X2 = x2
                else:
                    X1 = np.vstack((X1, x1))
                    X2 = np.vstack((X2, x2))
                    
            for epoch in range(num_epochs + 1):
                alpha = self.getLearningRate(learning_rate, epoch, drop, drop_epochs)

                if epoch > 0:
                    sess.run(optimiser, feed_dict={tf_X1:X1, tf_X2:X2, tf_alpha: alpha})
                    
                loss, Z = sess.run([tf_Loss, tf_Z], feed_dict = {tf_X1:X1, tf_X2:X2, tf_X:Y})
                
                Z[:, 2] = np.abs(Z[:, 2])
        
                epochs[epoch] = epoch
                losses[epoch] = loss
                alphas[epoch] = alpha
        
                print('Epoch {:d}: loss = {:.4f}, alpha = {:.6f}.'.format(epochs[epoch], losses[epoch], alpha))
                if epoch > 0:
                    #fig = plt.figure()
                    plt.figure(figsize=(8, 4))
                    ax = plt.axes(projection='3d')
                    ax.set_xlim(-1, 1)
                    ax.set_ylim(-1, 1)
                    ax.set_zlim(0, 1)
                    #ax.set_zlim(-1, 1)
                    ax.scatter3D(Z[:, 0], Z[:, 1], Z[:, 2],  c=self.color)
                        
                    plt.title('Epoch {:d}: loss = {:.4f}.'.format(epochs[epoch], losses[epoch]))
                    plt.savefig('{}/emb_epoch_{}.png'.format(embedding_folder, epoch))
                    plt.show()
                    
                if epoch == num_epochs:
                    np.save('{}/dome.npy'.format(embedding_folder), Z)
                    
            
            fig, subplot1 = plt.subplots()
            subplot1.set_xlabel('Epoch')
            subplot1.set_ylabel('Loss')
            subplot1.plot(epochs[1:num_epochs+1], losses[1:num_epochs+1], 'r-', linewidth=2.0)
            
            subplot2 = subplot1.twinx()
            subplot2.plot(epochs[1:num_epochs+1], alphas[1:num_epochs+1], 'b-', linewidth=1.0)
            subplot2.set_ylabel('Learning rate')
            
            red_line = mlines.Line2D([], [], color='red', label='Poincare')
            blue_line = mlines.Line2D([], [], color='blue', label='Learning rate')
            plt.legend(handles=[red_line, blue_line])
            #plt.title('Total time = {:.2f} s.'.format(embedding_delta_time)) 
            plt.savefig('{}/emb_loss.png'.format(embedding_folder))
            plt.show()
                    
            
    def runRandomWalk(self):
        self.V = list(self.G.nodes())
        self.n = len(self.V)
        self.VI = np.arange(self.n).tolist()

        print('--> Walking')
        print('Number of nodes: {}'.format(self.n))
        print('Number of walks: {}'.format(self.n * self.num_walks))
        print('Data size: {}'.format(self.n * self.num_walks * self.walk_length))
        
        print('Random walking in progress...')
        self.walks = graph.build_deepwalk_corpus(self.G, 
                                                 num_paths=self.num_walks,
                                                 path_length=self.walk_length,
                                                 alpha=0, 
                                                 rand=random.Random(self.seed))
        self.walks = [[self.V.index(int(node)) for node in walk] for walk in self.walks]

        print('Generating dataset from walks...')
        self.dataset = []
        for walk in self.walks:
            yjn_cdd = [item for item in self.VI if item not in walk]
            
            for i in range(self.window_size, len(walk)-self.window_size):
                yi = walk[i]
                yjp = walk[i-self.window_size:i+self.window_size+1]
                yjp = np.unique(yjp)
                yjp = [item for item in yjp if item != yi]
                
                np.random.RandomState(self.seed).shuffle(yjn_cdd)
                yjn = yjn_cdd[0:len(yjp)]
                
                params = np.zeros([2+len(yjp)*2], dtype=np.int32)
                params[0] = len(yjp)
                params[1] = yi
                params[2:2+len(yjp)] = yjp
                params[2+len(yjp):2+len(yjp)*2] = yjn
                
                self.dataset.append(params)

        print('---')
        
    def getLoss(self, Y, params):
        num_pos = params[0]
        num_neg = params[0]
        num_w = num_pos + num_neg

        Yi = tf.reshape(tf.tile(Y[0, :], [num_w]), [num_w, self.D])        
        Yj = Y[1:, :]
        
        norm_ij = tf.norm(Yi - Yj, axis=1) + self.epsilon
        if self.euclidean == False:
            norm_i = tf.norm(Yi, axis=1)
            norm_j = tf.norm(Yj, axis=1)
            zeta = 1 + 2 * self.R * norm_ij * norm_ij / (self.R - norm_i * norm_i + self.epsilon) / (self.R - norm_j * norm_j + self.epsilon)
            zeta = tf.clip_by_value(zeta, clip_value_min=1+self.epsilon, clip_value_max=np.inf)
            dist = tf.acosh(zeta)
        else:
        	dist = norm_ij

        flag1 = tf.concat([tf.ones([num_pos], dtype=tf.float32), -1 * tf.ones([num_neg], dtype=tf.float32)], 0)
        flag2 = tf.concat([1.0 / tf.cast(num_pos, tf.float32) * tf.ones([num_pos], dtype=tf.float32), self.gamma / tf.cast(num_neg, tf.float32) * tf.ones([num_neg], dtype=tf.float32)], 0)
        
        return tf.reduce_sum(tf.nn.softplus(tf.pow(dist, self.r) * flag1) * flag2, axis=0)

    def getEmbeddingDelta(self, Y, alpha, params):
        num_pos = params[0]
        num_neg = params[0]
        num_w = num_pos + num_neg

        Yi = tf.reshape(tf.tile(Y[0, :], [num_w]), [num_w, self.D])        
        Yj = Y[1:, :]

        norm_ij = tf.norm(Yi - Yj, axis=1) + self.epsilon
        if self.euclidean == False:
            norm_i = tf.norm(Yi, axis=1)
            norm_j = tf.norm(Yj, axis=1)
            zeta = 1 + 2 * self.R * norm_ij * norm_ij / (self.R - norm_i * norm_i + self.epsilon) / (self.R - norm_j * norm_j + self.epsilon)
            zeta = tf.clip_by_value(zeta, clip_value_min=1+self.epsilon, clip_value_max=np.inf)
            dist = tf.acosh(zeta)
        else:
            dist = norm_ij
        
        flag1 = tf.concat([tf.ones([num_pos], dtype=tf.float32), -1 * tf.ones([num_neg], dtype=tf.float32)], 0)
        dLdD = tf.reshape(tf.tile(tf.nn.sigmoid(tf.pow(dist, self.r) * flag1) * self.r * np.power(dist, self.r - 1), [self.D]), [self.D, num_w])
        dLdD = tf.transpose(dLdD)
        
        if self.euclidean == False:
	        k1 = tf.reshape(tf.tile(4 * self.R / tf.sqrt(zeta * zeta - 1) / (self.R - norm_i * norm_i  + self.epsilon) / (self.R - norm_j * norm_j + self.epsilon), [self.D]), [self.D, num_w])
	        k1 = tf.transpose(k1)
	        
	        a_ij = tf.reshape(tf.tile(norm_ij * norm_ij / (self.R - norm_i * norm_i + self.epsilon), [self.D]), [self.D, num_w])
	        a_ij = tf.transpose(a_ij)

	        a_ji = tf.reshape(tf.tile(norm_ij * norm_ij / (self.R - norm_j * norm_j + self.epsilon), [self.D]), [self.D, num_w])
	        a_ji = tf.transpose(a_ji)
	                
	        dDdi = k1 * (Yi - Yj + a_ij * Yi)
	        dDdj = k1 * (Yj - Yi + a_ji * Yj)
        else:
            Norm_ij = tf.reshape(tf.tile(norm_ij, [self.D]), [self.D, num_w])
            Norm_ij = tf.transpose(Norm_ij)
            
            dDdi = (Yi - Yj) / Norm_ij
            dDdj = (Yj - Yi) / Norm_ij
        
        flag2 = tf.reshape(tf.tile(tf.concat([1.0 / tf.cast(num_pos, tf.float32) * tf.ones([num_pos], dtype=tf.float32), -self.gamma / tf.cast(num_neg, tf.float32) * tf.ones([num_neg], dtype=tf.float32)], 0), [self.D]), [self.D, num_w])
        flag2 = tf.transpose(flag2)
        
        dLdi = dLdD * dDdi * flag2
        dLdj = dLdD * dDdj * flag2
        
        norm = tf.norm(Y, axis=1)
        k2 = (self.R - norm * norm) * (self.R - norm * norm) / 4
        k2 = tf.reshape(tf.tile(k2, [self.D]), [self.D, num_w + 1])
        k2 = tf.transpose(k2)
            
        dLdy = tf.concat([[tf.reduce_sum(dLdi, axis=0)], dLdj], axis=0)
        delta = alpha * k2 * dLdy
        return delta
    
    def getLearningRate(self, initial_learning_rate, epoch, drop, drop_epochs):
        alpha = initial_learning_rate
        if epoch > 0:
            alpha = initial_learning_rate * np.power(drop, np.floor((epoch - 1) / drop_epochs))
        
        return alpha                
        
    def embed(self, learning_rate=0.1, drop=0.99, drop_epochs=1):     
        h_flag = 'h'
        if self.euclidean == True:
            h_flag = 'e'
        
#        embedding_start_time = time.time()
#        embedding_start_time = int(round(embedding_start_time))
        embedding_folder = 'emb-{}-{}-{}-{}-{}'.format(self.folder_name, h_flag, self.num_walks, self.walk_length, self.window_size)    
        if not os.path.exists(embedding_folder):
            os.makedirs(embedding_folder)
            
        embedding_delta_time = 0
            
        np.save('{}/nodes.npy'.format(embedding_folder), self.V)
        np.save('{}/nodes_index.npy'.format(embedding_folder), self.VI)

        Y = self.epsilon * np.random.RandomState(self.seed).randn(self.n, self.D)

        losses = np.zeros(self.epoch + 1, dtype=np.float32)
        alphas = np.zeros(self.epoch + 1, dtype=np.float32)
        epochs = np.zeros(self.epoch + 1, dtype=np.int32)

        tf_Y = tf.placeholder(shape=[None, self.D], dtype=tf.float32)
        tf_params = tf.placeholder(shape=[None], dtype=tf.int32)
        tf_alpha = tf.placeholder(dtype=tf.float32)
        tf_EmbeddingDelta = self.getEmbeddingDelta(tf_Y, tf_alpha, tf_params)
        tf_Loss = self.getLoss(tf_Y, tf_params)
    
        init = tf.global_variables_initializer()
        with tf.Session() as sess:
            sess.run(init)
            print('--- Embedding with Tensorflow...')
            
            pca = PCA(n_components=2)
            
            for epoch in range(self.epoch + 1):
                loss_epoch = 0
                alpha_epoch = self.getLearningRate(learning_rate, epoch, drop, drop_epochs)
                epoch_start_time = time.time()
                
                if self.batch_size > 1:
                    batches = int(len(self.dataset) / self.batch_size)
                    
                    for i in range(batches):
                        if epoch > 0:
                            for j in range(i*self.batch_size, (i+1)*self.batch_size):
                                params = self.dataset[j]
                                samples = Y[params[1:], :]
                                delta = sess.run(tf_EmbeddingDelta, feed_dict={tf_Y: samples, tf_alpha: alpha_epoch, tf_params: params})                                
                                Y[params[1:], :] -= delta
                    
                            Y_norm = np.transpose(np.tile(np.linalg.norm(Y, axis=1), [self.D, 1]))
                            Y = Y * (Y_norm < self.SqrtR).astype(int) + Y * (Y_norm >= self.SqrtR).astype(int) * self.SqrtR / (Y_norm + self.epsilon)
                        
                        for j in range(i*self.batch_size, (i+1)*self.batch_size):
                            params = self.dataset[j]
                            samples = Y[params[1:], :]
                            loss = sess.run(tf_Loss, feed_dict={tf_Y: samples, tf_params: params})
                            loss_epoch += loss

                else:
                    for params in self.dataset:
                        samples = Y[params[1:], :]
                        
                        if epoch > 0:
                            delta = sess.run(tf_EmbeddingDelta, feed_dict={tf_Y: samples, tf_alpha: alpha_epoch, tf_params: params})
                            Y[params[1:], :] -= delta
                            
                            Y_norm = np.transpose(np.tile(np.linalg.norm(Y[params[1:], :], axis=1), [self.D, 1]))
                            Y[params[1:], :] = Y[params[1:], :] * (Y_norm < self.SqrtR).astype(int) + Y[params[1:], :] * (Y_norm >= self.SqrtR).astype(int) * self.SqrtR / (Y_norm + self.epsilon)

                        loss = sess.run(tf_Loss, feed_dict={tf_Y: samples, tf_params: params})
                        loss_epoch += loss

                losses[epoch] = loss_epoch
                alphas[epoch] = alpha_epoch
                epochs[epoch] = epoch
                epoch_delta_time = time.time() - epoch_start_time
                embedding_delta_time += epoch_delta_time

                print('Epoch {:d}: loss = {:.4f}, learning rate = {:.6f}, time = {:.2f} s.'.format(epochs[epoch], losses[epoch], alphas[epoch], epoch_delta_time))
                np.save('{}/emb_epoch_{}.npy'.format(embedding_folder, epoch), Y)
                
                if epoch > 0:
                    fig = plt.figure(figsize=(8, 8))
                    plt.title('Epoch {:d}: loss = {:.4f}, learning rate = {:.6f}, time = {:.2f} s.'.format(epochs[epoch], losses[epoch], alphas[epoch], epoch_delta_time)) 
        
                    Y2 = pca.fit_transform(Y)
                    plt.scatter(Y2[:, 0], Y2[:, 1], c=self.color)
                    
                    fig.savefig('{}/emb_epoch_{}_pca.png'.format(embedding_folder, epoch))
                    plt.show()
                    
#                if epoch == num_epochs:
#                    fig = plt.figure(figsize=(8, 8))
#                    plt.title('Epoch {:d}: loss = {:.4f}, learning rate = {:.6f}, time = {:.2f} s.'.format(epochs[epoch], losses[epoch], alphas[epoch], epoch_delta_time)) 
#
#                    tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300, method='exact')
#                    Y2 = tsne.fit_transform(Y)
#
#                    plt.scatter(Y2[:, 0], Y2[:, 1], c=self.color)
#                    
#                    fig.savefig('{}/emb_epoch_{}_tsne.png'.format(embedding_folder, epoch))
#                    #plt.show()
                
        print('Total time = {:.2f} s.'.format(embedding_delta_time))
      
        fig, subplot1 = plt.subplots()
        subplot1.set_xlabel('Epoch')
        subplot1.set_ylabel('Loss')
        subplot1.plot(epochs[1:self.epoch+1], losses[1:self.epoch+1], 'r-', linewidth=2.0)
        
        subplot2 = subplot1.twinx()
        subplot2.plot(epochs[1:self.epoch+1], alphas[1:self.epoch+1], 'b-', linewidth=1.0)
        subplot2.set_ylabel('Learning rate')
        
        red_line = mlines.Line2D([], [], color='red', label='Poincare')
        blue_line = mlines.Line2D([], [], color='blue', label='Learning rate')
        plt.legend(handles=[red_line, blue_line])
        plt.title('Total time = {:.2f} s.'.format(embedding_delta_time)) 
        fig.savefig('{}/emb_loss.png'.format(embedding_folder))
        plt.show()
        
        self.embedding = Y
        np.save('{}/embedding.npy'.format(embedding_folder), self.embedding)
        
        self.learning_rates = alphas
        np.save('{}/learning_rates.npy'.format(embedding_folder), self.learning_rates)
        
        self.losses = losses
        np.save('{}/losses.npy'.format(embedding_folder), self.losses)
        
        print('---')
                            
def main():
    parser = ArgumentParser("deepwalk",
                            formatter_class=ArgumentDefaultsHelpFormatter, 
                            conflict_handler='resolve')
      
    parser.add_argument('--format',
                        default='edgelist',
                        help='File format of input file')
    
    parser.add_argument('--input',
                        default='facebook_combined',
                        help='Input folder')
    
    parser.add_argument('--number_walks',
                        default=1,
                        type=int, 
                        help='Number of random walks to start at each node')
      
    parser.add_argument('--dimension', 
                        default=64, 
                        type=int, 
                        help='Number of latent dimensions to learn for each node.')
      
    parser.add_argument('--seed',
                        default=0, 
                        type=int,
                        help='Seed for random walk generator.')
    
    parser.add_argument('--undirected',
                        default=True,
                        type=bool,
                        help='Treat graph as undirected.')
    
    parser.add_argument('--walk_length',
                        default=20,
                        type=int,
                        help='Length of the random walk started at each node')
    
    parser.add_argument('--window_size',
                        default=5,
                        type=int,
                        help='Window size of skipgram model.')
    
    parser.add_argument('--epoch',
                        default=20,
                        type=int,
                        help='Number of epochs.')
    
    parser.add_argument('--euclidean',
                        default=False,
                        type=bool,
                        help='Type embedding.')
    
    parser.add_argument('--batch_size',
                        default=32,
                        type=int,
                        help='Number of data per batch.')
    
    args = parser.parse_args()
    #print(args)

    model = HyperbolicEmbeddding(
            folder=args.input, 
            graph_format=args.format,
            undirected=args.undirected,
            num_walks=args.number_walks,
            walk_length=args.walk_length,
            window_size=args.window_size,
            dim=args.dimension,
            seed=args.seed,
            epoch=args.epoch,
            euclidean=args.euclidean,
            batch_size=args.batch_size)

    model.runRandomWalk()
    model.embed()

#    Y = np.load('data/facebook_combined_poincare.npy')
#    model.embedToDome(Y, model.V)

if __name__ == "__main__":
    main()


# run hyperbolic_embedding.py --input facebook_combined --number_walks 1 --dimension 64 --walk_length 20 --window_size 5 --epoch 20 --batch_size 1
# python3 hyperbolic_embedding.py --input sx-mathoverflow-LCC --number_walks 10 --dimension 128 --walk_length 80 --window_size 10 --epoch 20 --batch_size 1