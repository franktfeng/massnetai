import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA

#XE = np.load('data/facebook_combined_poincare.npy')
XE = np.load('examples/emb_h_CA-AstroPh-LCC_1551073410/embedding.npy')
colors = np.load('data/CA-AstroPh-LCC/colors.npy')
ids = np.load('data/CA-AstroPh-LCC/ids.npy')

#model = TSNE(n_components=2, method='exact')
model = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=1200, method='exact')
#model = PCA(n_components=2)
YE = model.fit_transform(XE)
#np.save('data/facebook_combined_poincare_pca.npy', YE)


plt.figure(figsize=(8, 8))
plt.scatter(YE[:, 0], YE[:, 1], c=colors)
plt.show()